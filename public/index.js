let selectedFile = null;

function handleFile() {
    const fileInput = document.getElementById('fileInput');
    const file = fileInput.files[0];
    if (file) {
        selectedFile = file;
        const reader = new FileReader();
        reader.onload = function (e) {
            const fileContents = e.target.result;
            document.getElementById('fileContents').textContent = fileContents;
        };
        reader.readAsText(file);
    } else {
        alert('Please select a file to upload.');
    }
}

function removeFile() {
    if (selectedFile) { // Check if a file is selected
        selectedFile = null;
        document.getElementById('fileInput').value = '';
        document.getElementById('fileContents').textContent = '';
    }
    else {
       
            alert('No file selected to remove.');
            // return; // Exit the function if no file is selected
        }
    
    
}